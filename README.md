This library aim to create utility to create Value Object with ready made or custom validation rules, where composition promoted over inheritance.

This is raw library that in development process and not ready for production. Also, your feedback is appreciated.

### Root classes that extended from genereic SpecialThing class:

- SpecialString
- specialInt
- SpecialFloat
- SpecialBool
- SpecialDateTime

Example:

```
#[Regex("/^[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9af]{12}$/")]
class UUID extends SpecialString
{
}

$uuid = new UUID('25572f23-8355-4f09-8470-bc0a6df970a4');

$uuid->sameAs(new UUID('d15daeb1-af5e-4912-80e4-72863b911008')); //false
```

Also it is possible customise exception by implementing buildError() method:

```
#[Regex("/^[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9af]{12}$/")]
class UUID extends SpecialString
{
    protected function buildError($source, Throwable $reason): Throwable
    {
        return new UUIDError(
            sprintf(
                "Invalid UUID %s",
                $source,
            ),
            previous: $reason,
        );
    }
}
```

### Rules (Attribute classes)
- 	AllRules (logical AND)
- 	AnyRule (logical OR)
- 	DateTimeRange
- 	Equal
- 	FloatRange
- 	IntRange
- 	Length
- 	LengthRange
- 	LessThan
- 	LessThanOrEqual
- 	MoreThan
- 	MoreThanOrEqual
- 	Not
- 	NotEmpty
- 	NotEqual
- 	NotSameAs
- 	RFCEmail
- 	Regex
- 	SameAs
- 	TypeBool
- 	TypeFloat
- 	TypeInt
- 	TypeOf
- 	TypeString








