<?php

declare(strict_types=1);

namespace Tests\Unit\Iskras\ValueObjects;

use Iskras\ValueObjects\Core\SpecialString;
use Iskras\ValueObjects\Throwables\UUIDError;
use Iskras\ValueObjects\UUID;
use Tests\Unit\TestCaseHelper;

final class UUIDTest extends TestCaseHelper
{
    public function test_that_class_is_instance_of_SpecialString_class(): void
    {
        $this->assertTrue(
            condition: is_a(UUID::class, SpecialString::class, true),
            message: "Class should extend SpecialString class.",
        );
    }

    /**
     * @return array<int, array<int,string>>
     */
    public function getValidUUID(): array
    {
        return [
            ["123e4567-e89b-12d3-a456-426614174000"],
        ];
    }

    /**
     * Testing validate method with valid UUID examples.
     * Should not throw error, expecting void.
     *
     * @doesNotPerformAssertions
     *
     * @dataProvider getValidUUID
     */
    public function test_validate_method_with_valid_uuid(string $uuid): void
    {
        UUID::validate($uuid);
    }

    /**
     * @return array<int, array<int,string>>
     */
    public function getInValidUUID(): array
    {
        return [
            ["dump string"],
        ];
    }

    /**
     * @dataProvider getInValidUUID
     */
    public function test_validate_method_with_inValid_uuid(string $uuid): void
    {
        $this->expectException(UUIDError::class);
        $this->expectExceptionMessage("Invalid UUID: " . $uuid);

        UUID::validate($uuid);
    }
}
