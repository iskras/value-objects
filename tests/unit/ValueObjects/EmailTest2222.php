<?php

declare(strict_types=1);

namespace Tests\Unit\Iskras\ValueObjects;

use Iskras\ValueObjects\Core\SpecialString;
use Iskras\ValueObjects\Email;
use Iskras\ValueObjects\Throwables\EmailError;
use Tests\Unit\TestCaseHelper;

final class EmailTest extends TestCaseHelper
{
    public function test_that_class_is_instance_of_SpecialString_class(): void
    {
        $this->assertTrue(
            condition: is_a(Email::class, SpecialString::class, true),
            message: "Class should extend SpecialString class.",
        );
    }

    /**
     * @doesNotPerformAssertions
     */
    public function test_validate_method_with_unicode_email(): void
    {
        $validEmail = "müllörəşı@домейн.com";

        new Email($validEmail);
    }

    public function test_class_with_invalid_email(): void
    {
        $invalidEmail = "invalid email";

        $this->expectException(EmailError::class);
        $this->expectExceptionMessage("Invalid email: " . $invalidEmail);

        Email::validate($invalidEmail);
    }
}
