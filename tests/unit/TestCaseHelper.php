<?php

declare(strict_types=1);

namespace Iskras\Tests\Unit;

use Attribute;
use Faker\Generator;
use PHPUnit\Framework\TestCase;
use ReflectionAttribute;
use ReflectionClass;

class TestCaseHelper extends TestCase
{
    /** @inheritDoc */
    protected $backupStaticAttributes;

    /** @inheritDoc */
    protected $runTestInSeparateProcess;

    /** PHP faker for generating data for testing. */
    protected static Generator $faker;

    /**
     * Preparing test helpers.
     */
    public static function setUpBeforeClass(): void
    {
        self::$faker = \Faker\Factory::create();
    }

    /**
     * Custom assertion for testing class method is final.
     *
     * @phpstan-param class-string $className
     */
    public function assertClassMethodFinal(string $className, string $methodName): void
    {
        $reflection = new ReflectionClass($className);
        $methodIsFinal = $reflection->getMethod($methodName)->isFinal();

        $this->assertThat(
            $methodIsFinal,
            $this->isTrue(),
            sprintf(
                "Failed to assert that class method %s::%s is final.",
                $className,
                $methodName,
            )
        );
    }

    /**
     * Custom assertion for testing class constructor is final.
     *
     * @phpstan-param class-string $className
     */
    public function assertClassConstructorFinal(string $className): void
    {
        $reflection = new ReflectionClass($className);
        $methodIsFinal = $reflection->getConstructor()->isFinal();

        self::assertThat(
            $methodIsFinal,
            self::isTrue(),
            sprintf(
                "Failed to assert that constructor of class %s is final.",
                $className,
            )
        );
    }

    /**
     * Custom assertion for testing that class is attribute
     *
     * @phpstan-param class-string $className
     */
    public function assertClassIsAttribute(string $className): void
    {
        $reflection = new ReflectionClass($className);
        $attributes = $reflection->getAttributes(
            Attribute::class,
            ReflectionAttribute::IS_INSTANCEOF
        );

        self::assertThat(
            !empty($attributes),
            self::isTrue(),
            sprintf(
                "Failed to assert that class %s is attribute class.",
                $className,
            )
        );
    }

    /**
     * Custom assertion for testing that class is attribute with
     * default setting, that is targets all and not repeatable.
     *
     * @phpstan-param class-string $className
     */
    public function assertClassIsAttributeWithDefaultSettings(string $className): void
    {
        $reflection = new ReflectionClass($className);
        $attributes = $reflection->getAttributes(
            Attribute::class,
            ReflectionAttribute::IS_INSTANCEOF
        );

        $attributes = !$attributes ?: $attributes[0]->getArguments();

        self::assertClassIsAttribute($className);

        self::assertThat(
            count($attributes) == 0 || $attributes[0] === 63,
            self::isTrue(),
            sprintf(
                "Attribute %s should target all and should not be repeatable.",
                $className,
            )
        );
    }
}
