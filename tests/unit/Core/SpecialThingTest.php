<?php

declare(strict_types=1);

namespace Iskras\Tests\Unit\ValueObjects\Core;

use Exception;
use Iskras\Tests\Unit\TestCaseHelper;
use Iskras\Tests\Unit\ValueObjects\Core\Mocks\SpecialThingMock;
use Iskras\ValueObjects\Core\SpecialThing;
use ReflectionClass;

/**
 * @coversDefaultClass Iskras\ValueObjects\Core\SpecialThing
 */
final class SpecialThingTest extends TestCaseHelper
{
    /**
     * Constructor should be final to promote composition over inheritance.
     */
    public function test_constructor_is_final(): void
    {
        $this->assertClassConstructorFinal(SpecialThing::class);
    }

    /**
     * @covers ::source
     */
    public function test_source_method_is_final(): void
    {
        $this->assertClassConstructorFinal(SpecialThing::class, "source");
    }

    /**
     * @covers ::source
     */
    public function test_source_method_returns_static_class(): void
    {
        $mock = new SpecialThingMock("any");

        $reflection = new ReflectionClass(SpecialThingMock::class);
        $reflection->getMethod("source")->setAccessible(true);

        $new = $reflection->getMethod("source")->invoke($mock, "any");

        $this->assertInstanceOf(
            expected: SpecialThing::class,
            actual: $new,
        );
    }

    /**
     * Test that constructors uses handleSourceBeforeSet method
     * and handles source before setting to property.
     */
    public function test_constructor_calls_handleSourceBeforeSet_method_and_handles_source(): void
    {
        $originalString = self::$faker->realTextBetween(30, 40);
        $handledString = self::$faker->realTextBetween(10, 20);

        $mock = $this->getMockForAbstractClass(
            SpecialThing::class,
            callOriginalConstructor: false,
        );

        $mock->expects($this->once())
            ->method("handleSourceBeforeSet")
            ->willThrowException(new Exception($handledString));

        $this->expectExceptionMessage($handledString);

        $mock->__construct($originalString);
    }

    /**
     * @covers ::toSource
     */
    public function test_toSource_method_is_final(): void
    {
        $this->assertClassConstructorFinal(SpecialThing::class, "toSource");
    }

    /**
     * @covers ::toSource
     *
     * toSource method should handle source with handleSourceBeforeReturn method
     * before returning.
     */
    public function test_toSource_method_calls_handleSourceBeforeReturn_method_and_returns_handled_source(): void
    {
        $originalString = self::$faker->realTextBetween(30, 40);
        $handledString = self::$faker->realTextBetween(10, 20);

        $mock = new SpecialThingMock("any");
        $mock->setHandledSourceBeforeReturn($handledString);

        $reflection = new ReflectionClass(SpecialThingMock::class);
        $reflection->getMethod("toSource")->setAccessible(true);

        $returnedSource = $reflection->getMethod("toSource")->invoke($mock, $originalString);

        $this->assertSame(
            expected: $handledString,
            actual: $returnedSource,
        );
    }
}
