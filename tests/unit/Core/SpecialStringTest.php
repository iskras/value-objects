<?php

declare(strict_types=1);

namespace Iskras\Tests\Unit\ValueObjects\Core;

use Iskras\Tests\Unit\TestCaseHelper;
use Iskras\ValueObjects\Core\SpecialThing;
use Iskras\ValueObjects\Core\SpecialString;
use stdClass;
use TypeError;

/**
 * @coversDefaultClass \Iskras\ValueObjects\Core\SpecialString
 */
class SpecialStringTest extends TestCaseHelper
{
    public function test_instance_of_SpecialThing_class(): void
    {
        $this->assertTrue(
            condition: is_subclass_of(
                SpecialString::class,
                SpecialThing::class,
                true,
            ),
            message: "Class should be instance of " . SpecialSting::class . ".",
        );
    }

    public function test_instantiation_with_string(): void
    {
        $randomString = self::$faker->text();

        $new = new SpecialString($randomString);

        $this->assertInstanceOf(
            expected: SpecialString::class,
            actual: $new,
        );
    }

    public function test_type_error_with_non_string_argument(): void
    {
        $this->expectException(TypeError::class);

        new SpecialString(new stdClass);
    }

    /**
     * @covers ::handleSourceBeforeSet
     * @covers ::handleSourceBeforeReturn
     */
    public function test_source_handling_before_set_and_return(): void
    {
        $randomString = self::$faker->text();

        $new = new SpecialString($randomString);

        $this->assertSame(
            expected: $randomString,
            actual: $new->toString(),
        );
    }

    /**
     * @covers ::sameAs
     */
    public function test_sameAs_method_with_similar_object(): void
    {
        $randomString = self::$faker->text();

        $instanceOne = new SpecialString($randomString);
        $instanceTwo = new SpecialString($randomString);

        $this->assertTrue(
            condition: $instanceOne->sameAs($instanceTwo),
            message: "Objects with same initial source should be assumed as same",
        );
    }

    /**
     * @covers ::sameAs
     */
    public function test_sameAs_method_with_self_but_different_source(): void
    {
        $randomStringOne = self::$faker->realTextBetween(50, 100);
        $randomStringTwo = self::$faker->realTextBetween(150, 200);

        $instanceOne = new SpecialString($randomStringOne);
        $instanceTwo = new SpecialString($randomStringTwo);

        $this->assertFalse(
            condition: $instanceOne->sameAs($instanceTwo),
            message: "Objects with different initial source should be assumed as different objects",
        );
    }

    /**
     * @covers ::sameAs
     */
    public function test_sameAs_method_with_non_self_argument(): void
    {
        $randomString = self::$faker->text();

        $instance= new SpecialString($randomString);
        $argument = new stdClass;

        $this->expectException(TypeError::class);

        $instance->sameAs($argument);
    }
}
