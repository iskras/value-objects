<?php

declare(strict_types=1);

namespace Iskras\Tests\Unit\ValueObjects\Core;

use Iskras\Tests\Unit\TestCaseHelper;
use Iskras\ValueObjects\Core\SpecialThing;
use Iskras\ValueObjects\Core\SpecialFloat;
use stdClass;
use TypeError;

/**
 * @coversDefaultClass \Iskras\ValueObjects\Core\SpecialFloat
 */
class SpecialFloatTest extends TestCaseHelper
{
    public function test_instance_of_abstractSpecialThing_class(): void
    {
        $this->assertTrue(
            condition: is_subclass_of(
                SpecialFloat::class,
                SpecialThing::class,
                true,
            ),
            message: "Class should be instance of " . AbstractSpecialSting::class . ".",
        );
    }

    public function test_instantiation_with_float(): void
    {
        $randomFloat = self::$faker->randomFloat();

        $new = new SpecialFloat($randomFloat);

        $this->assertInstanceOf(
            expected: SpecialFloat::class,
            actual: $new,
        );
    }

    public function test_type_error_with_non_float_argument(): void
    {
        $this->expectException(TypeError::class);

        new SpecialFloat(new stdClass);
    }

    /**
     * @covers ::handleSourceBeforeSet
     * @covers ::handleSourceBeforeReturn
     */
    public function test_source_handling_before_set_and_return(): void
    {
        $randomFloat = self::$faker->randomFloat();

        $new = new SpecialFloat($randomFloat);

        $this->assertSame(
            expected: $randomFloat,
            actual: $new->toFloat(),
        );
    }

    /**
     * @covers ::sameAs
     */
    public function test_sameAs_method_with_similar_object(): void
    {
        $randomFloat = self::$faker->randomFloat(null, 100, 200);

        $instanceOne = new SpecialFloat($randomFloat);
        $instanceTwo = new SpecialFloat($randomFloat);

        $this->assertTrue(
            condition: $instanceOne->sameAs($instanceTwo),
            message: "Objects with same initial source should be assumed as same",
        );
    }

    /**
     * @covers ::sameAs
     */
    public function test_sameAs_method_with_self_but_different_source(): void
    {
        $randomFloatOne = self::$faker->randomFloat(null, 100, 200);
        $randomFloatTwo = self::$faker->randomFloat(null, 300, 400);

        $instanceOne = new SpecialFloat($randomFloatOne);
        $instanceTwo = new SpecialFloat($randomFloatTwo);

        $this->assertFalse(
            condition: $instanceOne->sameAs($instanceTwo),
            message: "Objects with different initial source should be assumed as different objects",
        );
    }

    /**
     * @covers ::sameAs
     */
    public function test_sameAs_method_with_non_self_argument(): void
    {
        $randomFloat = self::$faker->randomFloat(null, 100, 200);

        $instance = new SpecialFloat($randomFloat);
        $argument = new stdClass;

        $this->expectException(TypeError::class);

        $instance->sameAs($argument);
    }
}
