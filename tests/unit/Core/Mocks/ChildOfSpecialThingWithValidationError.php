<?php

declare(strict_types=1);

namespace Iskras\Tests\Unit\ValueObjects\Core\Mocks;

use Iskras\ValueObjects\Core\SpecialThing;

#[ValidationRuleMock()]
#[ValidationRuleMock("first_validation_error_from_instance_with_error")]
#[ValidationRuleMock("second_validation_error_from_instance_with_error")]
class ChildOfSpecialThingWithValidationError extends SpecialThing
{
    use ImplementSpecialThingTrait;
}
