<?php

declare(strict_types=1);

namespace Iskras\Tests\Unit\ValueObjects\Core\Mocks;

use DateTimeInterface;
use Iskras\ValueObjects\Core\SpecialThing;

class SpecialThingMock extends SpecialThing
{
    use ImplementSpecialThingTrait;

    private int|float|string|bool|array|DateTimeInterface|SpecialThing $handledSourceBeforeReturn;

    public function setHandledSourceBeforeReturn(mixed $handledSourceBeforeReturn): void
    {
        $this->handledSourceBeforeReturn = $handledSourceBeforeReturn;
    }

    protected function handleSourceBeforeReturn(
        $source
    ): int|float|string|bool|array|DateTimeInterface|SpecialThing {
        return $this->handledSourceBeforeReturn;
    }
}
