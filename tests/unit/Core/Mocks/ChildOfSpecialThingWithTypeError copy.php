<?php

declare(strict_types=1);

namespace Iskras\Tests\Unit\ValueObjects\Core\Mocks;

use Iskras\ValueObjects\Core\SpecialThing;

#[ValidationRuleMock("validation_error_from_instance_with_type_error")]
#[TypeCheckMock("type_error_from_instance_with_type_error")]
class ChildOfSpecialThingWithTypeAndValidationError extends SpecialThing
{
    use ImplementSpecialThingTrait;
}
