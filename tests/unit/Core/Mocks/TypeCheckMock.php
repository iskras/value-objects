<?php

declare(strict_types=1);

namespace Iskras\Tests\Unit\ValueObjects\Core\Mocks;

use Attribute;
use Iskras\ValueObjects\Core\Interfaces\TypeCheckInterface;
use TypeError;

#[Attribute(Attribute::TARGET_ALL|Attribute::IS_REPEATABLE)]
class TypeCheckMock implements TypeCheckInterface
{
    public function __construct(
        private ?string $message = null,
    ) {
    }

    public function try(mixed $argument): ?TypeError
    {
        return $this->message === null ? null : new TypeError($this->message);
    }
}
