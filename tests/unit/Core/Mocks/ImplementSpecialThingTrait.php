<?php

declare(strict_types=1);

namespace Iskras\Tests\Unit\ValueObjects\Core\Mocks;

use DateTimeInterface;
use Iskras\ValueObjects\Core\SpecialThing;

trait ImplementSpecialThingTrait
{
    public function handleSourceBeforeSet(
        int|float|string|bool|array|DateTimeInterface|SpecialThing $source
    ): int|float|string|bool|array|DateTimeInterface|SpecialThing {
        return $source;
    }

    public function handleSourceBeforeReturn(
        int|float|string|bool|array|DateTimeInterface|SpecialThing $source
    ): int|float|string|bool|array|DateTimeInterface|SpecialThing {
        return $source;
    }
}
