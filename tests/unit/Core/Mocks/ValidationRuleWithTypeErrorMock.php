<?php

declare(strict_types=1);

namespace Iskras\Tests\Unit\ValueObjects\Core\Mocks;

use Attribute;
use Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface;
use Throwable;
use TypeError;

#[Attribute()]
class ValidationRuleWithTypeErrorMock implements ValidationRuleInterface
{
    public function __construct(
        private ?string $message = null,
    ) {
    }

    public function try(mixed $argument): ?Throwable
    {
        return new TypeError($this->message);
    }
}
