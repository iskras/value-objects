<?php

declare(strict_types=1);

namespace Iskras\Tests\Unit\ValueObjects\Core\Mocks;

use Attribute;
use Exception;
use Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface;
use Throwable;

#[Attribute(Attribute::TARGET_ALL|Attribute::IS_REPEATABLE)]
class ValidationRuleWithArgumentMessageMock implements ValidationRuleInterface
{
    public function try(mixed $argument): ?Throwable
    {
        return new Exception($argument);
    }
}
