<?php

declare(strict_types=1);

namespace Iskras\Tests\Unit\ValueObjects\Core\Mocks;

use Attribute;
use Exception;
use Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface;
use Throwable;

#[Attribute(Attribute::TARGET_ALL|Attribute::IS_REPEATABLE)]
class ValidationRuleMock implements ValidationRuleInterface
{
    public function __construct(
        private ?string $message = null,
    ) {
    }

    public function try(mixed $argument): ?Throwable
    {
        return $this->message === null ? null : new Exception($this->message);
    }
}
