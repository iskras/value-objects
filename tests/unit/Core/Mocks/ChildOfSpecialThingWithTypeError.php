<?php

declare(strict_types=1);

namespace Iskras\Tests\Unit\ValueObjects\Core\Mocks;

use Iskras\ValueObjects\Core\SpecialThing;

#[TypeCheckMock()]
#[TypeCheckMock("first_type_error_from_instance_with_type_error")]
#[TypeCheckMock("second_type_error_from_instance_with_type_error")]
class ChildOfSpecialThingWithTypeError extends SpecialThing
{
    use ImplementSpecialThingTrait;
}
