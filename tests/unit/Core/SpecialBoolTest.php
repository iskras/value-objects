<?php

declare(strict_types=1);

namespace Iskras\Tests\Unit\ValueObjects\Core;

use Iskras\Tests\Unit\TestCaseHelper;
use Iskras\ValueObjects\Core\SpecialThing;
use Iskras\ValueObjects\Core\SpecialBool;
use stdClass;
use TypeError;

/**
 * @coversDefaultClass \Iskras\ValueObjects\Core\SpecialBool
 */
class SpecialBoolTest extends TestCaseHelper
{
    public function test_instance_of_abstractSpecialThing_class(): void
    {
        $this->assertTrue(
            condition: is_subclass_of(
                SpecialBool::class,
                SpecialThing::class,
                true,
            ),
            message: "Class should be instance of " . AbstractSpecialSting::class . ".",
        );
    }

    public function test_instantiation_with_bool_true(): void
    {
        $new = new SpecialBool(true);

        $this->assertInstanceOf(
            expected: SpecialBool::class,
            actual: $new,
        );
    }

    public function test_instantiation_with_bool_false(): void
    {
        $new = new SpecialBool(false);

        $this->assertInstanceOf(
            expected: SpecialBool::class,
            actual: $new,
        );
    }

    public function test_type_error_with_non_bool_argument(): void
    {
        $this->expectException(TypeError::class);

        new SpecialBool(new stdClass);
    }

    /**
     * @covers ::handleSourceBeforeSet
     * @covers ::handleSourceBeforeReturn
     */
    public function test_source_handling_before_set_and_return(): void
    {
        $bool = true;

        $new = new SpecialBool($bool);

        $this->assertSame(
            expected: $bool,
            actual: $new->toBoolean(),
        );
    }

    /**
     * @covers ::sameAs
     */
    public function test_sameAs_method_with_similar_object(): void
    {
        $bool = true;

        $instanceOne = new SpecialBool($bool);
        $instanceTwo = new SpecialBool($bool);

        $this->assertTrue(
            condition: $instanceOne->sameAs($instanceTwo),
            message: "Objects with same initial source should be assumed as same",
        );
    }

    /**
     * @covers ::sameAs
     */
    public function test_sameAs_method_with_self_but_different_source(): void
    {
        $boolOne = true;
        $boolTwo = false;

        $instanceOne = new SpecialBool($boolOne);
        $instanceTwo = new SpecialBool($boolTwo);

        $this->assertFalse(
            condition: $instanceOne->sameAs($instanceTwo),
            message: "Objects with different initial source should be assumed as different objects",
        );
    }

    /**
     * @covers ::sameAs
     */
    public function test_sameAs_method_with_non_self_argument(): void
    {
        $instance= new SpecialBool(true);
        $argument = new stdClass;

        $this->expectException(TypeError::class);

        $instance->sameAs($argument);
    }
}
