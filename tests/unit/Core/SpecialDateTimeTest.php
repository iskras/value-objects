<?php

declare(strict_types=1);

namespace Iskras\Tests\Unit\ValueObjects\Core;

use Iskras\Tests\Unit\TestCaseHelper;
use Iskras\ValueObjects\Core\SpecialThing;
use Iskras\ValueObjects\Core\SpecialDateTime;
use ReflectionClass;
use stdClass;
use TypeError;

/**
 * @coversDefaultClass \Iskras\ValueObjects\Core\SpecialDateTime
 */
class SpecialDateTimeTest extends TestCaseHelper
{
    public function test_instance_of_abstractSpecialThing_class(): void
    {
        $this->assertTrue(
            condition: is_subclass_of(
                SpecialDateTime::class,
                SpecialThing::class,
                true,
            ),
            message: "Class should be instance of " . AbstractSpecialSting::class . ".",
        );
    }

    public function test_instantiation_with_DateTime(): void
    {
        $randomDateTime = self::$faker->dateTime();

        $new = new SpecialDateTime($randomDateTime);

        $this->assertInstanceOf(
            expected: SpecialDateTime::class,
            actual: $new,
        );
    }

    public function test_type_error_with_non_int_argument(): void
    {
        $this->expectException(TypeError::class);

        new SpecialDateTime(new stdClass);
    }

    /**
     * Source should be cloned before set to property.
     *
     * @covers ::handleSourceBeforeSet
     */
    public function test_handleSourceBeforeSet_clones_source(): void
    {
        $randomDateTime = self::$faker->dateTime();

        $new = new SpecialDateTime($randomDateTime);

        $reflection = new ReflectionClass($new);
        $handleSourceBeforeSet = $reflection->getMethod("handleSourceBeforeSet");
        $handleSourceBeforeSet->setAccessible(true);

        $clonedValue = $handleSourceBeforeSet->invoke($new, $randomDateTime);

        $this->assertEquals(
            expected: $randomDateTime,
            actual: $clonedValue,
        );

        $this->assertNotSame(
            expected: $randomDateTime,
            actual: $clonedValue,
        );
    }

    /**
     * Source property should be cloned before returning.
     *
     * @covers ::handleSourceBeforeReturn
     */
    public function test_handleSourceBeforeReturn_method_clones_source(): void
    {
        $randomDateTime = self::$faker->dateTime();

        $new = new SpecialDateTime($randomDateTime);

        $reflection = new ReflectionClass($new);
        $handleSourceBeforeSet = $reflection->getMethod("handleSourceBeforeReturn");
        $handleSourceBeforeSet->setAccessible(true);

        $clonedValue = $handleSourceBeforeSet->invoke($new, $randomDateTime);

        $returnedValue = $new->toDateTime();

        $this->assertEquals(
            expected: $clonedValue,
            actual: $returnedValue,
        );

        $this->assertNotSame(
            expected: $clonedValue,
            actual: $returnedValue,
        );
    }

    /**
     * @covers ::sameAs
     */
    public function test_sameAs_method_with_similar_object(): void
    {
        $randomDateTime = self::$faker->dateTime();

        $instanceOne = new SpecialDateTime($randomDateTime);
        $instanceTwo = new SpecialDateTime($randomDateTime);

        $this->assertTrue(
            condition: $instanceOne->sameAs($instanceTwo),
            message: "Objects with same initial source should be assumed as same",
        );
    }

    /**
     * @covers ::sameAs
     */
    public function test_sameAs_method_with_self_but_different_source(): void
    {
        $randomDateTimeOne = self::$faker->dateTimeBetween("-1 year", "now");
        $randomDateTimeTwo = self::$faker->dateTimeBetween("+2 year", "+3 years");


        $instanceOne = new SpecialDateTime($randomDateTimeOne);
        $instanceTwo = new SpecialDateTime($randomDateTimeTwo);

        $this->assertFalse(
            condition: $instanceOne->sameAs($instanceTwo),
            message: "Objects with different initial source should be assumed as different objects",
        );
    }

    /**
     * @covers ::sameAs
     */
    public function test_sameAs_method_with_non_self_argument(): void
    {
        $randomDateTime = self::$faker->dateTime();

        $instance= new SpecialDateTime($randomDateTime);
        $argument = new stdClass;

        $this->expectException(TypeError::class);

        $instance->sameAs($argument);
    }
}
