<?php

declare(strict_types=1);

namespace Iskras\Tests\Unit\ValueObjects\Core\Rules;

use DateTime;
use Iskras\Tests\Unit\TestCaseHelper;
use Iskras\ValueObjects\Core\Rules\TypeOf;
use stdClass;
use TypeError;

/**
 * @coversDefaultClass TypeOf
 */
class TypeOfTest extends TestCaseHelper
{
    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function test_instantiation(): void
    {
        new TypeOf(stdClass::class);
    }

    /**
     * @test
     * @covers ::try
     */
    public function value_with_correct_type_should_return_null(): void
    {
        $new = new TypeOf(stdClass::class);

        $result = $new->try(new stdClass);

        $this->assertNull($result);
    }

    /**
     * @test
     * @covers ::try
     */
    public function value_with_wrong_type_should_return_type_error_instance(): void
    {
        $expectedType = stdClass::class;

        $new = new TypeOf($expectedType);

        $value = new DateTime();

        $result = $new->try($value);

        $this->assertInstanceOf(
            expected: TypeError::class,
            actual: $result,
        );

        $this->assertStringContainsString(
            sprintf(
                "Expecting %s, %s given",
                $expectedType,
                get_debug_type($value),
            ),
            $result->getMessage(),
        );
    }

    /**
     * @test
     */
    public function make_sure_class_is_attribute_and_targets_all_and_not_repeatable(): void
    {
        $this->assertClassIsAttributeWithDefaultSettings(TypeOf::class);
    }
}
