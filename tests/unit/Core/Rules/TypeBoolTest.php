<?php

declare(strict_types=1);

namespace Iskras\Tests\Unit\ValueObjects\Core\Rules;

use Iskras\Tests\Unit\TestCaseHelper;
use Iskras\ValueObjects\Core\Rules\TypeBool;
use stdClass;
use TypeError;

/**
 * @coversDefaultClass TypeBool
 */
class TypeBoolTest extends TestCaseHelper
{
    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function test_instantiation(): void
    {
        new TypeBool();
    }

    /**
     * @test
     * @covers ::try
     */
    public function value_with_boolean_should_return_null(): void
    {
        $new = new TypeBool();

        $result = $new->try(true);

        $this->assertNull($result);
    }

    /**
     * @test
     * @covers ::try
     */
    public function value_with_non_boolean_should_return_type_error_instance(): void
    {
        $new = new TypeBool();

        $value = new stdClass;

        $result = $new->try($value);

        $this->assertInstanceOf(
            expected: TypeError::class,
            actual: $result,
        );

        $this->assertStringContainsString(
            sprintf(
                "Expecting boolean, %s given",
                get_debug_type($value),
            ),
            $result->getMessage(),
        );
    }

    /**
     * @test
     */
    public function make_sure_class_is_attribute_and_targets_all_and_not_repeatable(): void
    {
        $this->assertClassIsAttributeWithDefaultSettings(TypeBool::class);
    }
}
