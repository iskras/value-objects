<?php

declare(strict_types=1);

namespace Iskras\Tests\Unit\ValueObjects\Core\Rules;

use Iskras\Tests\Unit\TestCaseHelper;
use Iskras\ValueObjects\Core\Rules\TypeString;
use stdClass;
use TypeError;

/**
 * @coversDefaultClass TypeString
 */
class TypeStringTest extends TestCaseHelper
{
    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function test_instantiation(): void
    {
        new TypeString();
    }

    /**
     * @test
     * @covers ::try
     */
    public function value_with_string_should_return_null(): void
    {
        $new = new TypeString();

        $result = $new->try("any string");

        $this->assertNull($result);
    }

    /**
     * @test
     * @covers ::try
     */
    public function value_with_non_string_should_return_type_error_instance(): void
    {
        $new = new TypeString();

        $value = new stdClass;

        $result = $new->try($value);

        $this->assertInstanceOf(
            expected: TypeError::class,
            actual: $result,
        );

        $this->assertStringContainsString(
            sprintf(
                "Expecting string, %s given",
                get_debug_type($value),
            ),
            $result->getMessage(),
        );
    }

    /**
     * @test
     */
    public function make_sure_class_is_attribute_and_targets_all_and_not_repeatable(): void
    {
        $this->assertClassIsAttributeWithDefaultSettings(TypeString::class);
    }
}
