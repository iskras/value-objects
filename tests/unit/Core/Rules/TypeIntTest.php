<?php

declare(strict_types=1);

namespace Iskras\Tests\Unit\ValueObjects\Core\Rules;

use Iskras\Tests\Unit\TestCaseHelper;
use Iskras\ValueObjects\Core\Rules\TypeInt;
use stdClass;
use TypeError;

/**
 * @coversDefaultClass TypeInt
 */
class TypeIntTest extends TestCaseHelper
{
    /**
     * @test
     * @doesNotPerformAssertions
     */
    public function test_instantiation(): void
    {
        new TypeInt();
    }

    /**
     * @test
     * @covers ::try
     */
    public function value_with_integer_should_return_null(): void
    {
        $new = new TypeInt();

        $result = $new->try(5);

        $this->assertNull($result);
    }

    /**
     * @test
     * @covers ::try
     */
    public function value_with_non_integer_should_return_type_error_instance(): void
    {
        $new = new TypeInt();

        $value = new stdClass;

        $result = $new->try($value);

        $this->assertInstanceOf(
            expected: TypeError::class,
            actual: $result,
        );

        $this->assertStringContainsString(
            sprintf(
                "Expecting int, %s given",
                get_debug_type($value),
            ),
            $result->getMessage(),
        );
    }

    /**
     * @test
     */
    public function make_sure_class_is_attribute_and_targets_all_and_not_repeatable(): void
    {
        $this->assertClassIsAttributeWithDefaultSettings(TypeInt::class);
    }
}
