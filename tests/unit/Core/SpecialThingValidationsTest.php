<?php

declare(strict_types=1);

namespace Iskras\Tests\Unit\ValueObjects\Core;

use Iskras\Tests\Unit\TestCaseHelper;
use Iskras\Tests\Unit\ValueObjects\Core\Mocks\ChildOfSpecialThingWithTypeAndValidationError;
use Iskras\Tests\Unit\ValueObjects\Core\Mocks\ChildOfSpecialThingWithTypeError;
use Iskras\Tests\Unit\ValueObjects\Core\Mocks\ChildOfSpecialThingWithValidationError;
use Iskras\Tests\Unit\ValueObjects\Core\Mocks\ImplementSpecialThingTrait;
use Iskras\Tests\Unit\ValueObjects\Core\Mocks\TypeCheckMock;
use Iskras\Tests\Unit\ValueObjects\Core\Mocks\ValidationRuleMock;
use Iskras\Tests\Unit\ValueObjects\Core\Mocks\ValidationRuleWithTypeErrorMock;
use Iskras\ValueObjects\Core\SpecialThing;
use Iskras\ValueObjects\Core\Throwables\ValidationError;
use Throwable;
use TypeError;

/**
 * @coversDefaultClass Iskras\ValueObjects\Core\SpecialThing
 */
final class SpecialThingValidationsTest extends TestCaseHelper
{
    public function test_normal_instantiation_with_no_validation_error(): void
    {
        $mock = new #[ValidationRuleMock()] class("any") extends SpecialThing
        {
            use ImplementSpecialThingTrait;
        };

        $this->assertTrue(
            condition: $mock instanceof SpecialThing,
            message: "Should be instance of SpecialThing class."
        );
    }

    public function test_that_validation_rule_attributes_of_child_applies(): void
    {
        $this->expectExceptionMessage("validation_error");

        new #[ValidationRuleMock("validation_error")] class("any") extends SpecialThing
        {
            use ImplementSpecialThingTrait;
        };
    }

    public function test_that_validation_rule_attributes_of_child_applies_top_to_down(): void
    {
        $this->expectExceptionMessage("first_validation_error");

        new #[
            ValidationRuleMock(),
            ValidationRuleMock("first_validation_error"),
            ValidationRuleMock("second_validation_error")
        ] class("any") extends SpecialThing
        {
            use ImplementSpecialThingTrait;
        };
    }

    public function test_validation_rule_attributes_of_child_applies_before_parent_rules(): void
    {
        $this->expectExceptionMessage("validation_error_from_child");

        new #[
            ValidationRuleMock("validation_error_from_child")
        ] class("any") extends ChildOfSpecialThingWithValidationError
        {
        };
    }

    public function test_validation_rule_attributes_of_parent_applies_after_child_rules(): void
    {
        $this->expectExceptionMessage("first_validation_error_from_instance_with_error");

        new #[
            ValidationRuleMock()
        ] class("any") extends ChildOfSpecialThingWithValidationError
        {
        };
    }

    public function test_validation_rule_attributes_of_parent_applies_top_to_down(): void
    {
        $this->expectExceptionMessage("first_validation_error_from_instance_with_error");

        new class("any") extends ChildOfSpecialThingWithValidationError
        {
        };
    }

    public function test_type_check_attributes_of_child_applies(): void
    {
        $this->expectExceptionMessage("type_error");

        new #[TypeCheckMock("type_error")] class("any") extends SpecialThing
        {
            use ImplementSpecialThingTrait;
        };
    }

    public function test_type_check_attributes_of_child_applies_top_to_down(): void
    {
        $this->expectExceptionMessage("first_type_error");

        new #[
            TypeCheckMock(),
            TypeCheckMock("first_type_error"),
            TypeCheckMock("second_type_error"),
        ] class("any") extends SpecialThing
        {
            use ImplementSpecialThingTrait;
        };
    }

    public function test_type_check_attributes_of_child_applies_before_parent_type_checks(): void
    {
        $this->expectExceptionMessage("type_error_from_child");

        new #[
            TypeCheckMock("type_error_from_child")
        ] class("any") extends ChildOfSpecialThingWithTypeError
        {
        };
    }

    public function test_type_check_attributes_of_parent_applies_before_child_rules(): void
    {
        $this->expectExceptionMessage("first_type_error_from_instance_with_type_error");

        new #[
            TypeCheckMock()
        ] class("any") extends ChildOfSpecialThingWithTypeError
        {
        };
    }

    public function test_type_check_attributes_of_parent_applies_top_to_down(): void
    {
        $this->expectExceptionMessage("first_type_error_from_instance_with_type_error");

        new class("any") extends ChildOfSpecialThingWithTypeError
        {
        };
    }

    public function test_type_check_attributes_of_child_applies_before_any_validation_rule(): void
    {
        $this->expectExceptionMessage("type_error");

        new #[
            ValidationRuleMock("validation_error"),
            TypeCheckMock("type_error"),
        ] class("any") extends SpecialThing
        {
            use ImplementSpecialThingTrait;
        };
    }

    public function test_type_check_attributes_of_parent_applies_before_any_validation_rule(): void
    {
        $this->expectExceptionMessage("type_error_from_instance_with_type_error");

        new class("any") extends ChildOfSpecialThingWithTypeAndValidationError
        {
        };
    }

    public function test_type_check_attributes_of_child_before_any_type_check_of_parent(): void
    {
        $this->expectExceptionMessage("type_error_from_child");

        new #[
            TypeCheckMock("type_error_from_child")
        ] class("any") extends ChildOfSpecialThingWithTypeAndValidationError
        {
        };
    }

    /**
     * @covers ::buildError
     */
    public function test_buildErrorMethod_called_on_validation_error(): void
    {
        $randomString = self::$faker->text();

        $this->expectExceptionMessage($randomString);

        new #[ValidationRuleMock("some_error")] class($randomString) extends SpecialThing
        {
            use ImplementSpecialThingTrait;

            protected function buildError(mixed $source, Throwable $reason): Throwable
            {
                return new ValidationError($source);
            }
        };
    }

    public function test_TypeError_from_validation_attributes_thrown_as_is(): void
    {
        $this->expectException(TypeError::class);
        $this->expectExceptionMessage("Type error");

        new #[ValidationRuleWithTypeErrorMock("Type error")] class("any") extends SpecialThing
        {
            use ImplementSpecialThingTrait;
        };
    }
}
