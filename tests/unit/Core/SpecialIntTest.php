<?php

declare(strict_types=1);

namespace Iskras\Tests\Unit\ValueObjects\Core;

use Iskras\Tests\Unit\TestCaseHelper;
use Iskras\ValueObjects\Core\SpecialThing;
use Iskras\ValueObjects\Core\SpecialInt;
use stdClass;
use TypeError;

/**
 * @coversDefaultClass \Iskras\ValueObjects\Core\SpecialInt
 */
class SpecialIntTest extends TestCaseHelper
{
    public function test_instance_of_abstractSpecialThing_class(): void
    {
        $this->assertTrue(
            condition: is_subclass_of(
                SpecialInt::class,
                SpecialThing::class,
                true,
            ),
            message: "Class should be instance of " . AbstractSpecialSting::class . ".",
        );
    }

    public function test_instantiation_with_int(): void
    {
        $randomInt = self::$faker->randomNumber();

        $new = new SpecialInt($randomInt);

        $this->assertInstanceOf(
            expected: SpecialInt::class,
            actual: $new,
        );
    }

    public function test_type_error_with_non_int_argument(): void
    {
        $this->expectException(TypeError::class);

        new SpecialInt(new stdClass);
    }

    /**
     * @covers ::handleSourceBeforeSet
     * @covers ::handleSourceBeforeReturn
     */
    public function test_source_handling_before_set_and_return(): void
    {
        $randomInt = self::$faker->numberBetween(100, 200);

        $new = new SpecialInt($randomInt);

        $this->assertSame(
            expected: $randomInt,
            actual: $new->toInt(),
        );
    }

    /**
     * @covers ::sameAs
     */
    public function test_sameAs_method_with_similar_object(): void
    {
        $randomInt = self::$faker->numberBetween(100, 200);

        $instanceOne = new SpecialInt($randomInt);
        $instanceTwo = new SpecialInt($randomInt);

        $this->assertTrue(
            condition: $instanceOne->sameAs($instanceTwo),
            message: "Objects with same initial source should be assumed as same",
        );
    }

    /**
     * @covers ::sameAs
     */
    public function test_sameAs_method_with_self_but_different_source(): void
    {
        $randomIntOne = self::$faker->numberBetween(100, 200);
        $randomIntTwo = self::$faker->numberBetween(300, 400);

        $instanceOne = new SpecialInt($randomIntOne);
        $instanceTwo = new SpecialInt($randomIntTwo);

        $this->assertFalse(
            condition: $instanceOne->sameAs($instanceTwo),
            message: "Objects with different initial source should be assumed as different objects",
        );
    }

    /**
     * @covers ::sameAs
     */
    public function test_sameAs_method_with_non_self_argument(): void
    {
        $randomInt = self::$faker->numberBetween(100, 200);

        $instance= new SpecialInt($randomInt);
        $argument = new stdClass;

        $this->expectException(TypeError::class);

        $instance->sameAs($argument);
    }
}
