FROM php:8.1-fpm

#install debug tools
RUN pecl install xdebug && docker-php-ext-enable xdebug
RUN pecl install pcov && docker-php-ext-enable pcov


#install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

#install zip tools
RUN apt-get update && apt-get install -y \
       libzip-dev \
       zip \
    && docker-php-ext-install zip

ENV PATH="$PATH:/var/www/html/vendor/bin"
