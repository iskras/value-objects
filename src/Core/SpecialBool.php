<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core;

use Iskras\ValueObjects\Core\Interfaces\ComparableInterface;
use Iskras\ValueObjects\Core\Traits\PrimitiveCompareTrait;
use Iskras\ValueObjects\Core\Rules\TypeBool;

/**
 * @extends SpecialThing<bool>
 *
 * @method self __construct(bool $source)
 */
#[TypeBool()]
class SpecialBool extends SpecialThing implements ComparableInterface
{
    use PrimitiveCompareTrait;

    /**
     * @phpstan-pure
     *
     * @param bool $source
     */
    final protected function handleSourceBeforeSet($source): bool
    {
        return $source;
    }

    /**
     * @phpstan-pure
     *
     * @param bool $source
     */
    final protected function handleSourceBeforeReturn($source): bool
    {
        return $source;
    }

    final public function fromBoolean(bool $source): static
    {
        return $this->source($source);
    }

    final public function toBoolean(): bool
    {
        return $this->toSource();
    }
}
