<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core;

use DateTimeInterface;
use Iskras\ValueObjects\Core\Interfaces\TypeCheckInterface;
use Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface;
use Iskras\ValueObjects\Core\Throwables\ValidationError;
use ReflectionAttribute;
use ReflectionClass;
use Throwable;
use TypeError;

/**
 * Generic template class for creating readonly objects with single argument.
 * Main methods including constructor method are final to promote composition
 * over inheritance.
 *
 * Additionally, it is possible to validate source argument
 * by adding Attributes to child classes, as long as these attributes are
 * instances of TypeCheckInterface or ValidationRuleInterface.
 * Validation attributes applies to source argument from child to parent order,
 * so that attributes from child classes applies before those in parent class.
 * Also, validation attributes in each class applies from up to down.
 * Rules that implements TypeCheckInterface applies before all other validation
 * rules, from child to parent order and up to down in each class.
 *
 * @template ThingSource of int|float|string|bool|array|DateTimeInterface|self

 */
abstract class SpecialThing
{
    /**
     * @phpstan-var ThingSource
     */
    private readonly int|float|string|bool|array|DateTimeInterface|self $source;

    /**
     * @phpstan-param ThingSource $source
     */
    final public function __construct(
        int|float|string|bool|array|DateTimeInterface|self $source
    ) {
        $allValidationRules = [];
        $allTypeRules = [];

        $reflection = new ReflectionClass(static::class);

        while ($reflection !== false) {
            //populate type checks.
            $attributes = $reflection->getAttributes(
                TypeCheckInterface::class,
                ReflectionAttribute::IS_INSTANCEOF,
            );

            foreach ($attributes as $attribute) {
                array_push($allTypeRules, $attribute->newInstance());
            }

            //populate validation rules.
            $attributes = $reflection->getAttributes(
                ValidationRuleInterface::class,
                ReflectionAttribute::IS_INSTANCEOF,
            );

            $parentRules = [];

            foreach ($attributes as $attribute) {
                array_push($parentRules, $attribute->newInstance());
            }

            //push parent rules to the end,
            //assuring that child rules applies first.
            $allValidationRules = array_merge($allValidationRules, $parentRules);

            $reflection = $reflection->getParentClass();
        }

        //assure type checks will be handled first.
        $allRules = array_merge($allTypeRules, $allValidationRules);

        if (count($allRules) > 0) {
            $handledSource = $this->handleSourceBeforeSet($source);

            foreach ($allRules as $rule) {
                $error = $rule->try($handledSource);

                if ($error !== null) {
                    //throw TypeError without change.
                    if (!$error instanceof TypeError) {
                        $error = $this->buildError($source, $error);
                    }

                    throw $error;
                }
            }
        }

        $this->source = $this->handleSourceBeforeSet($source);
    }

    /**
     * @template ThingSourceStatic of ThingSource
     *
     * @phpstan-param ThingSourceStatic $source
     */
    final protected static function source(
        int|float|string|bool|array|DateTimeInterface|self $source
    ): static {
        return new static($source);
    }

    /**
     * @phpstan-return ThingSource
     */
    final protected function toSource(): int|float|string|bool|array|DateTimeInterface|self
    {
        return $this->handleSourceBeforeReturn($this->source);
    }

    /**
     * Defines how source value will be handled before validating and
     * setting to property. Good example is cloning before, to prevent mutation
     * from outside of class.
     *
     * @phpstan-param ThingSource $source
     * @phpstan-return ThingSource
     */
    abstract protected function handleSourceBeforeSet(
        int|float|string|bool|array|DateTimeInterface|self $source
    ): int|float|string|bool|array|DateTimeInterface|self;

    /**
     * Defines how source value will be handled before sending source property
     * to outside of class scope. Good example is cloning, to prevent mutation
     * from outside of class.
     *
     * @phpstan-param ThingSource $source
     * @phpstan-return ThingSource
     */
    abstract protected function handleSourceBeforeReturn(
        int|float|string|bool|array|DateTimeInterface|self $source
    ): int|float|string|bool|array|DateTimeInterface|self;

    /**
     * Returns Throwable object if validation attributes fails validating
     * source value.
     *
     * @phpstan-param ThingSource $source
     */
    protected function buildError(
        int|float|string|bool|array|DateTimeInterface|self $source,
        Throwable $reason,
    ): Throwable {
        return new ValidationError(
            sprintf(
                "Validation error occurred: %s",
                $reason->getMessage(),
            ),
            previous: $reason
        );
    }
}
