<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core;

use Iskras\ValueObjects\Core\Interfaces\ComparableInterface;
use Iskras\ValueObjects\Core\Traits\PrimitiveCompareTrait;
use Iskras\ValueObjects\Core\Rules\TypeInt;

/**
 * @extends SpecialThing<int>
 *
 * @method self __construct(int $source)
 */
#[TypeInt()]
class SpecialInt extends SpecialThing implements ComparableInterface
{
    use PrimitiveCompareTrait;

    /**
     * @phpstan-pure
     *
     * @param int $source
     */
    final protected function handleSourceBeforeSet($source): int
    {
        return $source;
    }

    /**
     * @phpstan-pure
     *
     * @param int $source
     */
    final protected function handleSourceBeforeReturn($source): int
    {
        return $source;
    }

    final public function fromInt(int $source): static
    {
        return $this->source($source);
    }

    final public function toInt(): int
    {
        return $this->toSource();
    }
}
