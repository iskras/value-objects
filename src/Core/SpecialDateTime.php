<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core;

use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Iskras\ValueObjects\Core\Interfaces\ComparableInterface;
use Iskras\ValueObjects\Core\Rules\TypeOf;
use TypeError;

/**
 * @extends SpecialThing<DateTime|DateTimeImmutable>
 *
 * @method self __construct(DateTime|DateTimeImmutable $source)
 */
#[TypeOf(DateTimeInterface::class)]
class SpecialDateTime extends SpecialThing implements ComparableInterface
{
    /**
     * @phpstan-pure
     *
     * @param DateTime|DateTimeImmutable $source
     */
    final protected function handleSourceBeforeSet($source): DateTime|DateTimeImmutable
    {
        return $source::createFromInterface($source);
    }

    /**
     * @phpstan-pure
     *
     * @param DateTime|DateTimeImmutable $source
     */
    final protected function handleSourceBeforeReturn($source): DateTime|DateTimeImmutable
    {
        return $source::createFromInterface($source);
    }

    final public function fromDateTime(DateTime|DateTimeImmutable $source): static
    {
        return $this->source($source);
    }

    final public function toDateTime(): DateTime|DateTimeImmutable
    {
        return $this->toSource();
    }

    /**
     * @inheritDoc
     *
     * @param static $argument
     * @throws TypeError When $argument type is not static::class
     */
    final public function sameAs(mixed $argument): bool
    {
        if ($argument instanceof static) {
            return $argument->toSource() == $this->toSource();
        }

        throw new TypeError(
            sprintf(
                "Expecting %s, %s given",
                $this::class,
                get_debug_type($argument),
            )
        );
    }
}
