<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\Traits;

use TypeError;

trait PrimitiveCompareTrait
{
    /**
     * @inheritDoc
     *
     * @param static $argument
     * @throws TypeError When $argument type is not static::class
     */
    final public function sameAs(mixed $argument): bool
    {
        if ($argument instanceof static) {
            return $argument->toSource() === $this->toSource();
        }

        throw new TypeError(
            sprintf(
                "Expecting %s, %s given",
                $this::class,
                get_debug_type($argument),
            )
        );
    }
}
