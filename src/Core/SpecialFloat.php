<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core;

use Iskras\ValueObjects\Core\Interfaces\ComparableInterface;
use Iskras\ValueObjects\Core\Traits\PrimitiveCompareTrait;
use Iskras\ValueObjects\Core\Rules\TypeFloat;

/**
 * @extends SpecialThing<float>
 *
 * @method self __construct(float $source)
 */
#[TypeFloat()]
class SpecialFloat extends SpecialThing implements ComparableInterface
{
    use PrimitiveCompareTrait;

    /**
     * @phpstan-pure
     *
     * @param float $source
     */
    final protected function handleSourceBeforeSet($source): float
    {
        return $source;
    }

    /**
     * @phpstan-pure
     *
     * @param float $source
     */
    final protected function handleSourceBeforeReturn($source): float
    {
        return $source;
    }

    final public function fromFloat(float $source): static
    {
        return $this->source($source);
    }

    final public function toFloat(): float
    {
        return $this->toSource();
    }
}
