<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core;

use Iskras\ValueObjects\Core\Interfaces\ComparableInterface;
use Iskras\ValueObjects\Core\Traits\PrimitiveCompareTrait;
use Iskras\ValueObjects\Core\Rules\TypeString;
use Stringable;

/**
 * @extends SpecialThing<string>
 *
 * @method self __construct(string $source)
 */
#[TypeString()]
class SpecialString extends SpecialThing implements Stringable, ComparableInterface
{
    use PrimitiveCompareTrait;

    /**
     * @phpstan-pure
     *
     * @param string $source
     */
    final protected function handleSourceBeforeSet($source): string
    {
        return $source;
    }

    /**
     * @phpstan-pure
     *
     * @param string $source
     */
    final protected function handleSourceBeforeReturn($source): string
    {
        return $source;
    }

    final public function fromString(string $source): static
    {
        return $this->source($source);
    }

    final public function toString(): string
    {
        return $this->toSource();
    }

    final public function __toString(): string
    {
        return $this->toSource();
    }
}
