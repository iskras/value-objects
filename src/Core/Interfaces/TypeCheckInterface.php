<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\Interfaces;

use TypeError;

/**
 * @template TypeToCheck
 */
interface TypeCheckInterface
{
    /**
     * @phpstan-param TypeToCheck $argument
     */
    public function try(mixed $argument): ?TypeError;
}
