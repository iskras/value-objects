<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\Interfaces;

use Throwable;

/**
 * @template TypeToValidate
 */
interface ValidationRuleInterface
{
    /**
     * @phpstan-param TypeToValidate $argument
     */
    public function try(mixed $argument): ?Throwable;
}
