<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\Interfaces;

/**
 * Interface to compare elements for equality.
 */
interface ComparableInterface
{
    /**
     * Method to check whether given argument
     * equals to/same as current object.
     *
     * @param static $argument
     */
    public function sameAs(mixed $argument): bool;
}
