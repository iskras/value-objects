<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\Throwables;

use ValueError;

/**
 * Intended for throwing when object arguments
 * do not meet validation rules.
 */
class ValidationError extends ValueError
{
}
