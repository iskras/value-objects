<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\Throwables;

class EmailRFCValidationError extends EmailValidationError
{
}
