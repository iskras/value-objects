<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\Throwables;

use Iskras\ValueObjects\Core\Throwables\ValidationError;

class EqualValidationError extends ValidationError
{
}
