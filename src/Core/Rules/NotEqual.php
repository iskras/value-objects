<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\ValidationRules;

use Attribute;
use Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface;
use Iskras\ValueObjects\Core\Throwables\NotEqualValidationError;
use Throwable;

/**
 * @implements ValidationRuleInterface<mixed>
 */
#[Attribute()]
class NotEqual implements ValidationRuleInterface
{
    public function __construct(
        private mixed $value,
    ) {
    }

    public function try(mixed $argument): ?Throwable
    {
        if ($argument == $this->value) {
            return new NotEqualValidationError("Values should not be equal");
        }

        return null;
    }
}
