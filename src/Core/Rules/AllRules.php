<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\ValidationRules;

use Attribute;
use Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface;
use Iskras\ValueObjects\Core\Throwables\ValidationError;
use Throwable;

#[Attribute()]
class AllRules implements ValidationRuleInterface
{
    /** @var ValidationRuleInterface[] */
    private array $rules;

    public function __construct(ValidationRuleInterface ...$rules)
    {
        $this->rules = $rules;
    }

    public function try(mixed $argument): ?Throwable
    {
        foreach ($this->rules as $rule) {
            $result = $rule->try($argument);

            if ($result !== null) {
                return new ValidationError(
                    "Not all validation rules met",
                    0,
                    $result,
                );
            }
        }

        return null;
    }
}
