<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\ValidationRules;

use Attribute;
use DateTimeInterface;
use Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface;
use Iskras\ValueObjects\Core\Throwables\DateTimeRangeValidationError;
use Throwable;

/**
 * @implements \Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface<DateTimeInterface>
 */
#[Attribute()]
class DateTimeRange implements ValidationRuleInterface
{
    public function __construct(
        private ?DateTimeInterface $start = null,
        private ?DateTimeInterface $end = null,
        private bool $startIncluded = true,
        private bool $endIncluded = true,
    ) {
    }

    /**
     * @param DateTimeInterface $argument
     */
    public function try($argument): ?Throwable
    {
        if (
            $argument < $this->start
            || $argument > $this->end
            || ($this->startIncluded === false && $argument == $this->start)
            || ($this->endIncluded === false && $argument == $this->end)
        ) {
            return new DateTimeRangeValidationError(
                sprintf(
                    "%s not in range of %s%s , %s%s",
                    $argument->format(DateTimeInterface::ATOM),
                    $this->startIncluded ? "[" : "(",
                    $this->start === null ? "-" : $this->start->format(DateTimeInterface::ATOM),
                    $this->end === null ? "-" : $this->end->format(DateTimeInterface::ATOM),
                    $this->endIncluded ? "]" : ")",
                ),
            );
        }

        return null;
    }
}
