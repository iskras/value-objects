<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\Rules;

use Attribute;
use Iskras\ValueObjects\Core\Interfaces\TypeCheckInterface;
use TypeError;

/**
 * Rule to check if type of given value is float.
 *
 * @implements TypeCheckInterface<mixed>
 */
#[Attribute()]
class TypeFloat implements TypeCheckInterface
{
    public function try(mixed $argument): ?TypeError
    {
        if (is_float($argument) === false) {
            return new TypeError(
                sprintf(
                    "Expecting float, %s given",
                    get_debug_type($argument),
                ),
            );
        }

        return null;
    }
}
