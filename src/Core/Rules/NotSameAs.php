<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\ValidationRules;

use Attribute;
use Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface;
use Iskras\ValueObjects\Core\Throwables\NotSameAsValidationError;
use Throwable;

/**
 * @implements ValidationRuleInterface<mixed>
 */
#[Attribute()]
class NotSameAs implements ValidationRuleInterface
{
    public function __construct(
        private mixed $value,
    ) {
    }

    public function try(mixed $argument): ?Throwable
    {
        if ($argument !== $this->value) {
            return new NotSameAsValidationError("Values should not be same");
        }

        return null;
    }
}
