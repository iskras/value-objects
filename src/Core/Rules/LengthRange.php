<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\ValidationRules;

use Attribute;
use Countable;
use Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface;
use Iskras\ValueObjects\Core\Throwables\LengthRangeValidationError;
use Throwable;

/**
 * @implements \Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface<string|array|Countable>
 */
#[Attribute()]
class LengthRange implements ValidationRuleInterface
{
    public function __construct(
        private int $min = PHP_INT_MIN,
        private int $max = PHP_INT_MAX,
        private bool $minIncluded = true,
        private bool $maxIncluded = true,
        private int $mode = COUNT_NORMAL,
    ) {
    }

    /**
     * @param string|array|Countable $argument
     */
    public function try($argument): ?Throwable
    {
        $length = is_string($argument) ? strlen($argument) : count($argument, $this->mode);

        if (
            $length < $this->min
            || $length > $this->max
            || ($this->minIncluded === false && $length === $this->min)
            || ($this->maxIncluded === false && $length === $this->max)
        ) {
            return new LengthRangeValidationError(
                sprintf(
                    "Length %s not in range of %s%s , %s%s",
                    $length,
                    $this->minIncluded ? "[" : "(",
                    $this->min,
                    $this->max,
                    $this->maxIncluded ? "]" : ")",
                ),
            );
        }

        return null;
    }
}
