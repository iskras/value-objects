<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\ValidationRules;

use Attribute;
use Countable;
use DateTimeInterface;
use Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface;
use Iskras\ValueObjects\Core\Throwables\MoreThanOrEqualValidationError;
use Throwable;

/**
 * @implements \Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface<string|int|float|DateTimeInterface>
 */
#[Attribute()]
class MoreThanOrEqual implements ValidationRuleInterface
{
    public function __construct(
        private int|float|DateTimeInterface $value,
    ) {
    }

    /**
     * @param string|int|float|array|Countable|DateTimeInterface $argument
     */
    public function try($argument): ?Throwable
    {
        if (is_string($argument) === true) {
            $argument = strlen($argument);
        } elseif (is_countable($argument)) {
            $argument = count($argument);
        }

        if ($argument < $this->value) {
            return new MoreThanOrEqualValidationError(
                sprintf(
                    "%s not less than or equal to %s",
                    $argument instanceof DateTimeInterface
                        ? $argument->format(DateTimeInterface::ATOM)
                        : $argument,
                    $this->value instanceof DateTimeInterface
                        ? $this->value->format(DateTimeInterface::ATOM)
                        : $this->value
                ),
            );
        }

        return null;
    }
}
