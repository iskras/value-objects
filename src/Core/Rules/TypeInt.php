<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\Rules;

use Attribute;
use Iskras\ValueObjects\Core\Interfaces\TypeCheckInterface;
use TypeError;

/**
 * Rule to check if type of given value is integer.
 *
 * @implements TypeCheckInterface<mixed>
 */
#[Attribute()]
class TypeInt implements TypeCheckInterface
{
    public function try(mixed $argument): ?TypeError
    {
        if (is_int($argument) === false) {
            return new TypeError(
                sprintf(
                    "Expecting int, %s given",
                    get_debug_type($argument),
                ),
            );
        }

        return null;
    }
}
