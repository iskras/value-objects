<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\ValidationRules;

use Attribute;
use Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface;
use Iskras\ValueObjects\Core\Throwables\ValidationError;
use Throwable;

#[Attribute()]
class Not implements ValidationRuleInterface
{
    public function __construct(
        private ValidationRuleInterface $rule,
    ) {
    }

    public function try(mixed $argument): ?Throwable
    {
        if ($this->rule->try($argument) === null) {
            return new ValidationError("Rule should not succeed");
        }

        return null;
    }
}
