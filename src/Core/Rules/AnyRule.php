<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\ValidationRules;

use Attribute;
use Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface;
use Iskras\ValueObjects\Core\Throwables\ValidationError;
use Throwable;

#[Attribute()]
class AnyRule implements ValidationRuleInterface
{
    /** @var ValidationRuleInterface[] */
    private array $rules;

    public function __construct(ValidationRuleInterface ...$rules)
    {
        $this->rules = $rules;
    }

    public function try(mixed $argument): ?Throwable
    {
        $failedRules = [];

        foreach ($this->rules as $rule) {
            $result = $rule->try($argument);

            if ($result === null) {
                return null;
            }

            array_push($failedRules, $result);
        }

        return new ValidationError(
            sprintf(
                "None of following validation rules met:" . PHP_EOL . "%s",
                implode(
                    PHP_EOL,
                    array_map(fn($f) => (string) $f, $failedRules)
                ),
            )
        );
    }
}
