<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\Rules;

use Attribute;
use Iskras\ValueObjects\Core\Interfaces\TypeCheckInterface;
use TypeError;

/**
 * Rule to check if type of given value is boolean.
 *
 * @implements TypeCheckInterface<mixed>
 */
#[Attribute()]
class TypeBool implements TypeCheckInterface
{
    public function try(mixed $argument): ?TypeError
    {
        if (is_bool($argument) === false) {
            return new TypeError(
                sprintf(
                    "Expecting boolean, %s given",
                    get_debug_type($argument),
                ),
            );
        }

        return null;
    }
}
