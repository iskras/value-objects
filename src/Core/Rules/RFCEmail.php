<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\ValidationRules;

use Attribute;
use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\EmailValidation;
use Egulias\EmailValidator\Validation\RFCValidation;
use Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface;
use Iskras\ValueObjects\Core\Throwables\EmailRFCValidationError;
use Throwable;

/**
 * @implements \Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface<string>
 *
 * RFCEmail is rule for Email according to broader RFC specifications.
 * Following specifications supported.
 * @link https://tools.ietf.org/html/rfc5321
 * @link https://tools.ietf.org/html/rfc5322
 * @link https://tools.ietf.org/html/rfc6530
 * @link https://tools.ietf.org/html/rfc6532
 * @link https://tools.ietf.org/html/rfc1035
 *
 * Relies on RFCValidation rule fo egulias\EmailValidator package.
 * @link https://github.com/egulias/EmailValidator/
 * @uses RFCValidation
 */
#[Attribute()]
class RFCEmail implements ValidationRuleInterface
{
    private EmailValidator $emailValidator;
    private EmailValidation $rule;

    public function __construct()
    {
        $this->emailValidator = new EmailValidator();
        $this->rule = new RFCValidation();
    }

    /**
     * @param string $argument
     */
    public function try($argument): ?Throwable
    {
        $result = $this->emailValidator->isValid(
            $argument,
            $this->rule,
        );

        if ($result === false) {
            return new EmailRFCValidationError(
                sprintf(
                    "String %s message do not match RFC email rules",
                    $argument,
                ),
            );
        }

        return null;
    }
}
