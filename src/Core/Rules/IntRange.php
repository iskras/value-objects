<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\ValidationRules;

use Attribute;
use Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface;
use Iskras\ValueObjects\Core\Throwables\IntRangeValidationError;
use Throwable;

/**
 * @implements \Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface<int>
 */
#[Attribute()]
class IntRange implements ValidationRuleInterface
{
    public function __construct(
        private int $min = PHP_INT_MIN,
        private int $max = PHP_INT_MAX,
        private bool $minIncluded = true,
        private bool $maxIncluded = true,
    ) {
    }

    /**
     * @param int $argument
     */
    public function try($argument): ?Throwable
    {
        if (
            $argument < $this->min
            || $argument > $this->max
            || ($this->minIncluded === false && $argument === $this->min)
            || ($this->maxIncluded === false && $argument === $this->max)
        ) {
            return new IntRangeValidationError(
                sprintf(
                    "%s not in range of %s%s , %s%s",
                    $argument,
                    $this->minIncluded ? "[" : "(",
                    $this->min,
                    $this->max,
                    $this->maxIncluded ? "]" : ")",
                ),
            );
        }

        return null;
    }
}
