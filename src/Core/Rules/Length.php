<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\ValidationRules;

use Attribute;
use Countable;
use Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface;
use Iskras\ValueObjects\Core\Throwables\LengthValidationError;
use Throwable;

/**
 * @implements ValidationRuleInterface<string|array|Countable>
 */
#[Attribute()]
class Length implements ValidationRuleInterface
{
    public function __construct(
        private int $length,
        private int $mode = COUNT_NORMAL,
    ) {
    }

    /**
     * @param string|array|Countable $argument
     */
    public function try($argument): ?Throwable
    {
        $length = is_string($argument) ? strlen($argument) : count($argument, $this->mode);

        if ( $length !== $this->length) {
            return new LengthValidationError(
                sprintf(
                    "Length %s is not equal to %s",
                    $length,
                    $this->length,
                ),
            );
        }

        return null;
    }
}
