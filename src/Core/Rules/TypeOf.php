<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\Rules;

use Attribute;
use Iskras\ValueObjects\Core\Interfaces\TypeCheckInterface;
use stdClass;
use TypeError;

/**
 * Rule to check if type of given value is
 * instance of specific type.
 *
 * @implements TypeCheckInterface<mixed>
 */
#[Attribute()]
class TypeOf implements TypeCheckInterface
{
    /**
     * @phpstan-param class-string $type
     */
    public function __construct(
        private string $type,
    ) {
    }

    public function try(mixed $argument): ?TypeError
    {
        if ($argument instanceof $this->type === false) {
            return new TypeError(
                sprintf(
                    "Expecting %s, %s given",
                    $this->type,
                    get_debug_type($argument),
                ),
            );
        }

        return null;
    }
}
