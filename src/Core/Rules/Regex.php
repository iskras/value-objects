<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\ValidationRules;

use Attribute;
use Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface;
use Iskras\ValueObjects\Core\Throwables\RegexValidationError;

/**
 * Validation rule for matching strings against
 * given regular expression.
 *
 * @implements ValidationRuleInterface<string>
 */
#[Attribute()]
class Regex implements ValidationRuleInterface
{
    public function __construct(
        private readonly string $regex
    ) {
    }

    /**
     * @param string $argument
     */
    public function try($argument): ?RegexValidationError
    {
        return $this->tryWithType($argument);
    }

    private function tryWithType(string $argument): ?RegexValidationError
    {
        if (preg_match($this->regex, $argument) === 1) {
            return new RegexValidationError(
                sprintf(
                    "String value do not match regex %s",
                    $this->regex,
                ),
            );
        }

        return null;
    }
}
