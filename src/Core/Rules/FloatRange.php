<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\ValidationRules;

use Attribute;
use Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface;
use Iskras\ValueObjects\Core\Throwables\FloatRangeValidationError;
use Throwable;

/**
 * @implements \Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface<float>
 */
#[Attribute()]
class FloatRange implements ValidationRuleInterface
{
    public function __construct(
        private float $min = PHP_FLOAT_MIN,
        private float $max = PHP_FLOAT_MAX,
        private bool $minIncluded = true,
        private bool $maxIncluded = true,
    ) {
    }

    /**
     * @param float $argument
     */
    public function try($argument): ?Throwable
    {
        if (
            $argument < $this->min
            || $argument > $this->max
            || ($this->minIncluded === false && $argument === $this->min)
            || ($this->maxIncluded === false && $argument === $this->max)
        ) {
            return new FloatRangeValidationError(
                sprintf(
                    "%s not in range of %s%s , %s%s",
                    $argument,
                    $this->minIncluded ? "[" : "(",
                    $this->min,
                    $this->max,
                    $this->maxIncluded ? "]" : ")",
                ),
            );
        }

        return null;
    }
}
