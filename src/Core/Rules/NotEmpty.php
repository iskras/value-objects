<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Core\ValidationRules;

use Attribute;
use Iskras\ValueObjects\Core\Interfaces\ValidationRuleInterface;
use Iskras\ValueObjects\Core\Throwables\NotEmptyValidationError;
use Throwable;

/**
 * @implements ValidationRuleInterface<mixed>
 */
#[Attribute()]
class NotEmpty implements ValidationRuleInterface
{
    public function try(mixed $argument): ?Throwable
    {
        if (empty($argument) === true) {
            return new NotEmptyValidationError("Value should not be empty");
        }

        return null;
    }
}
