<?php

declare(strict_types=1);

namespace Iskras\ValueObjects\Throwables;

use Iskras\ValueObjects\Core\Throwables\ValidationError;

class UUIDError extends ValidationError
{
}
