<?php

declare(strict_types=1);

namespace Iskras\ValueObjects;

use Iskras\ValueObjects\Core\SpecialString;
use Iskras\ValueObjects\Core\ValidationRules\Regex;
use Iskras\ValueObjects\Throwables\UUIDError;
use Throwable;

#[Regex("/^[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9af]{12}$/")]
class UUID extends SpecialString
{
    /**
     * @psalm-mutation-free
     *
     * @param string $source
     */
    protected function buildError($source, Throwable $reason): Throwable
    {
        return new UUIDError(
            sprintf(
                "Invalid UUID %s",
                $source,
            ),
            previous: $reason,
        );
    }
}
