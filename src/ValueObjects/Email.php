<?php

declare(strict_types=1);

namespace Iskras\ValueObjects;

use Iskras\ValueObjects\Core\SpecialString;
use Iskras\ValueObjects\Core\ValidationRules\RFCEmail;
use Iskras\ValueObjects\Throwables\EmailError;
use Throwable;

/**
 * Email value object.
 *
 * This class allowed to be extended for aliasing and additional functionality.
 * Note that constructor and other base methods are final to promote composition
 * over inheritance.
 *
 * @package Value Objects
 * @category Web
 *
 * @uses EmailRFCValidationRule
 * @see EmailRFCValidationRule
 */
#[RFCEmail]
class Email extends SpecialString
{
    /**
     * @psalm-mutation-free
     *
     * @param string $source
     */
    protected function buildError($source, Throwable $reason): Throwable
    {
        return new EmailError(
            sprintf(
                "Invalid email %s",
                $source,
            ),
            previous: $reason,
        );
    }
}
